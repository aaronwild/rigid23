\documentclass{lecnotes}

%%
\usepackage{style-lecnotes}
%%
\usepackage{custommath_gen}
\usepackage{category-names}
\usepackage{custommath_font}

\usepackage{custommath_rigid}

\usepackage{gitinfo2}
%%
\title{\normalsize{Rigid Analytic Geometry}}
\date{
  \normalsize{
  \color{red}
  Nightly Version 
  \\ --- \\ 
  expect errors, omissions and drastic changes!
  \bigbreak 
  \itshape{Date: \gitAuthorDate, Commit: \gitAbbrevHash, Version:\gitVtagn}
}
}
\addtokomafont{publishers}{\normalsize}
\publishers{Available at \url{aaronwild.gitlab.io/arila22/arila_Nightly.pdf}} %\linespread{1.1}
\addtokomafont{author}{\small}
\author{Lecture by Prof. J. Franke\\ Notes and Mistakes by Aaron Wild}
\bibliography{lit.bib}
\begin{document}
{
  \thispagestyle{plain}
  \maketitle
  \hypersetup{linkcolor=black, urlcolor = deeplilac}
  \tableofcontents
}
\thispagestyle{empty}
\chapter{Introduction and Prerequisites}
\section{Topological problems}
Problems: $\qqp$  and $\cc_p$ with their ordinary topology are totally disconnected, where $\cc_p$ is the (topological) completion of $\overline{\rat}_p$.
So the identity theorem for holomorphic functions is likely to fail (if holomorphic functions are supposed to form an ordinary sheaf).
Moreover, there is no higher sheaf cohomology.
There is a also a seemingly minor problem:
The ball
\[
  \mathbb{B}
  \defo
  \Set{x\in \cp \given \abs{x} \leq 1 }
\]
fails to be compact, as 
\[
  \mathbb{B}
  = 
  \bigcup_{\xi \in \mathbb{B} \setminus \idm}
  \xi + \idm 
\]
where $\idm = \Set{x\in \mathbb{B} \given \abs{x}}$ is the maximal ideal of the subring $\mathbb{B} \sse \cp$.
Now Tate's idea is to enforce the (quasi-)compactness of certain open subsets, and this idea was fully developed by the school around Grauert.
However, a number of problems remain: 
For example, if $K$ is a complete and algebraically closed field extension of $\cc_p$, there is no continous map $\mathbb{B}_{1,K} \to \mathbb{B}_{1,\cc_p}$ corresponding to 
\[
  \mathbb{T}_{\cc_p}
  = 
  \Set{\sum_{k=0}^\infty a_k T^k \given \text{$\abs{a_k} \to 0$ as $k \to \infty$}}
  \longrightarrow 
  T_K.
\]
Van der Put and Schneider-Van der Put (c.f. \cite{putrig}) introduced additional points.\footnote{Van der Put was apparently motivated by takling the ``corona problem'' of complex analytic geometry in the context of rigid geometry}
Ber\-ko\-vich and later Huber did even more.
\par 
In this course, we will work with Grothendieck topologies in the strict case, avoiding having to work with pretopologies.
\par 
In real algebraic geometry, one has a similar problem:
Recall that a field $K$ is called \emph{real closed} if it satisfies the following equivalent conditions:
\begin{enumerate}
  \item 
    $K$ can be ordered in precisely one way, and every polynomial $p\in K[T]$ with a sign change in $[a,b]$ has a zero in $[a,b]$,
  \item 
    $1 < [\overline{K}:K] < \infty$,
  \item 
    $\overline{K} = K(\sqrt{-1}) \neq K$.
\end{enumerate}
Let $F \sse \rr$ be the algebraic closure of $\rat$ in $\rr$, which is real closed.
Then:
\begin{enumerate}
  \item 
    $[0,1]_F$ is not connected for the euclidean topology, as it decomposes as 
    \[
      \left([0,\frac{\pi}{4}]_{\rr} \cap F\right)
      \cup 
      \left([\frac{\pi}{4},1]_{\rr} \cap F\right)
    \]
    This is a problem when we want to compute Betti numbers.
  \item 
    $[0,1]_F$ is not compact for similar reasons:
    \[
      [0,1]_F 
      = 
      \bigcup_{k=0}^{\infty}
      \left(
      [0,2^{-k} \lfloor 2^{k-2} \pi \rfloor ]_F 
      \right)
      \cup 
      \left(
      [2^{-k} \lceil 2^{k-2} \pi \rceil ,1]_F 
      \right).
    \]
\end{enumerate}
Tate's machinery for rigid spaces is similar to machinery devolped by Delfs/Knebusch in the context of real algebraic geometry.

\section{$G_+$-topological spaces and their van der Put points}
The notion of sieves was introduced by Giraud in his Bourbaki report \cite{giraud}.
\begin{defn}
  Let $\ccat$ be any category
  \begin{enumerate}
    \item 
      Let $X \in \ccat$ be an object.
      A \emph{sieve over $X$} is a class of morphisms $\ssieve = \Set{(v\mc V \to X)}$ with the following property:
      If $f\mc U \to V$ is a morphism in $\ccat$ and $(v\mc V \to X)$ is an element of $\mathcal{S}$, then $(vf\mc U \to X)$ is also an element in $\mathcal{S}$.\footnote{Observe that since sieves are stable under precomposition, they are particular stable under isomorphisms on the source.}
      In more concise terms, a sieve on $X$ is a subpresheaf of $\hom_{\ccat}(-,X)$. 
    \item 
      A \emph{Grothendieck Topology} $\mathfrak{T}$ on $\ccat$ associates to every $X \in \ccat$ a collection of seaves $\mathfrak{T}_X$, called \emph{covering sieves} of $X$, such that the following conditions hold:
      \begin{enumerate}
        \item
          GT-Triv: 
          The \emph{all-sieve} $\asieve$ (containing all morphisms with target $X$) is a covering sieve of $X$;
        \item
          GT-Trans: 
          If $\mathcal{S}\in \mathfrak{T}_X$ is a covering sieve of $X$ and $f\mc Y \to X$ is a morphism in $\ccat$, then the \emph{pull-back sieve}
          \[
            f^\ast 
            \mathcal{S}
            \defo 
            \Set{v\mc V \to Y\given fv \in \mathcal{S}}
          \]
          is a covering sieve of $Y$ (i.e. $f^\ast \mathcal{S} \in \mathfrak{T}_Y$).
        \item 
          GT-Loc:
          Let $\mathcal{T}$ be any $X$-sieve and let $\mathcal{S} \in \mathfrak{T}_X$ be a covering sieve of $X$ such that for all $(v\mc V \to X)\in \ssieve$, the pull-back sieve $v^\ast \mathcal{T}$ is a covering sieve of $V$.
          Then $\mathcal{T}$ is a covering sieve of $X$ (i.e. $\mathcal{T} \in \mathfrak{T}_X$).
      \end{enumerate}
  \end{enumerate}
\end{defn}

\begin{rem}
  Recall that a \emph{presheaf} on a category $\ccat$ is a contravariant functor from $\ccat$ to some (usually concrete) category.
  If $\ccat$ is equipped with a Grothendieck topology $\mathfrak{T}$, then a presheaf $\shg$ on $\ccat$ is called a \emph{$\mathfrak{T}$-sheaf} if for all $X\in \ccat$ and sieves $\mathcal{S}\in \mathfrak{T}_X$, the following conditions hold:
  the morphisms $v\mc \shg(X) \to \shg(V)$ induced by $(v\mc V \to X)$ in $\mathcal{S}$ induces a bijection 
  \[
    \shg(X) 
    \isomorphism 
    \lim_{v\in \mathcal{S}}
    \shg(v).
  \]
\end{rem}

\begin{rem}
  We are mostly interested in the case where the category $\ccat$ is the poset $\ouvcat(X)$ of open sets of a topological space $X$, i.e. the objects of $\ccat$ are given by open subsets $V\sse X$, and 
  \[
    \hom_{\ccat}(V',V) = 
    \begin{cases}
      \Set{\ast},
      & 
      \text{if $V'\sse V$};
      \\ 
      \emptyset, 
      &
      \text{otherwise}.
    \end{cases}
  \]
  Then a sieve over (or ``in'') $V$ is just a collection $\mathcal{S}$ of open subsets of $V$ such that $U \in \mathcal{S}$ and $W\sse U$ with $W$ open implies that $W\in \mathcal{S}$.
  The pull-back of a sieve $\ssieve$ on $V$ along a (unique if it exists) morphism $V' \sse V$ is just $\restrict{\mathcal{S}}{V'} = \mathcal{S} \cap \mathfrak{O}_{V'}$, where $\mathfrak{O}_{V'}$ is the set of open subsets of $V'$.
  We write $\mathcal{S} \seq V$ if $\mathcal{S}$ is a sieve in $V$ which covers $V$ in the given Grothendieck topology, if several Grothendieck topologies need to be distinguished, we write $\mathcal{S}\seq_{\mathfrak{T}} V$.
  We will also write $\mathcal{S} \cslash V'$ if $\restrict{\mathcal{S}}{V'} \seq V'$.
  \par 
  We warn the reader that in this setup, we deal with two somewhat different concepts, that both cary the name ``topology'': first, the (ordinary) topology on $X$, which determines the category $\ouvcat(X)$ and which stays fixed; on the other hand, possibly different Grothendieck topologies on $\ouvcat(X)$.  The later gives us a formalism for ``singeling out'' certain open subsets on $X$, and declaring when they should form a ``covering'' on $X$ (even though they might not form a cover in the ``ordinary'' topology).
  However, the ordinary topology on $X$ certainly does define a Grothendieck topology on $X$, i.e. we have: 
  \begin{inexample*}
    Let $X$ be a topology space.
    Let $\topt$ be the assignement that assigns to an open subset $V \sse X$ the collection
    \[
      \Set{\ssieve \given \ssieve \sse \Set{(U \sse V)~\mathrm{open}},~\bigcup_{U \in \ssieve} U = V}
    \]
    defines a Grothendieck topology on $\ouvcat(X)$.
  \end{inexample*}
\end{rem}

\begin{fact}
  \label{fact:sieves-basic}
  Let $\ccat$ be a category, $X$ an object of $\ccat$ and $\topt$ a Grothendieck topology on $\ccat$.
  \begin{enumerate}
    \item 
      \label{fact:sieves-basic:contain}
      Every sieve $\mathcal{S}$ containing a $\topt$-covering sieve $\mathcal{S}'$ of $X$ is itself a $\topt$-covering of $X$.
    \item 
      The intersection of two covering sieves is a covering.\footnote{Note that arbitrary intersection of sieves is again a sieve, but the same is not true for covering sieves.}
  \end{enumerate}
\end{fact}

\begin{proof}
  Since this is our first proof ot this sort, let us be explicit.
  We first make the following oberservations on the behavior of the pull-back operation on sieves:
  \begin{claim*}
    Let $\ssieve$ be a sieve on $X$, and $(v\mc V\to X)\in \ssieve$.
    Then the pull-back sieve $v^\ast \ssieve$ is the all-sieve on $V$.
    Let $f\mc Y \to X$ be a morphism in $\ccat$.
    For two sieves $\ssieve,\ssieve'$ on $X$, we have $f^\ast(\ssieve \cap \ssieve') = f^\ast\ssieve \cap f^\ast\ssieve'$.
    If, moreoever $\ssieve \sse \ssieve'$, then $f^\ast \ssieve \sse f^\ast \ssieve'$.
  \end{claim*}
  \begin{proof*}
    Recall that the pull-back sieve $v^\ast \ssieve$ is defined as 
    \[
       v^\ast \ssieve 
       \defo 
      \{\begin{tikzcd}[column sep = small, cramped] U \ar{r}[above]{u} & V\end{tikzcd} ~|~(\begin{tikzcd}[column sep = small, cramped] U \ar{r}[above]{u} & V \ar{r}[above]{v} & X \end{tikzcd})\in \ssieve\}.
    \]
    But, since being a member of a sieve is by definition stable under precomposition, any morphism $u\mc U \to V$ satisfies this condition.
    The second and third parts are obvious.
  \end{proof*}
  \begin{enumerate}
    \item 
      With this statement at our disposal, we can use GT-Loc to show that $\ssieve$ is also a sieve.
      Namely, for $(v\mc V \to X) \in \ssieve$, the pull-back sieve $v^\ast \ssieve$ is the all-sieve, and hence a $\topt$-covering sieve of $V$ by GT-Triv.
      Since this holds for any $v\in \ssieve'$ and $\ssieve$ is a $\topt$-covering sieve, GT-Trans indeed implies that $\ssieve'$ is also a sieve.
      (Note that we only needed $\ssieve$ to be a $\topt$-cover to use GT-Trans, not for applying the above claim)
    \item 
      Let $(v\mc V \to X) \in \ssieve$.
      Then, by the observation, we have that $v^\ast \ssieve$ is the all-sieve, and thus $v^\ast(\ssieve \cap \ssieve') = v^\ast(\ssieve) \cap v^\ast(\ssieve') = v^\ast \ssieve'$.
      The latter is a $\topt$-covering on $V$ by GT-Trans for $\ssieve'$.
      The claim then again follows from GT-Loc for $\ssieve$.
  \end{enumerate}
\end{proof}

\begin{defn}
  Let $X$ be a topological space and $(V_i)_{i\in I}$ an $I$-indexed collection of open subsets of a fixed subset $V$.
  We wil write $\left[V_i ~|~ i \in I \right]$ for the sieve over $V$ generated by the collection $(V_i)$.\footnote{Although we only defined it for topological spaces, this notion again makes sense for any category: Namely, if $(v_i\mc V_i \to V)$ is a fixed collection of morphisms with common target, then the sieve generated by the $(V_i)$ consists of morphisms $V'\to V$ that factor as $ V' \to  V_i \xrightarrow{v_i} V$ for some $i\in I$.}
  We have 
  \[
    \left[ V_i ~|~ i \in I\right]
    =
    \Set{U \in \mathfrak{O}_X \given \text{there is $i\in I$ such that $U \sse V_i$}}
    = 
    \bigcap_{\substack{\text{$\ssieve$ a sieve in $X$}\\ \ssieve \supseteq \Set{V_i \given i \in I}}}
    \ssieve.
  \]
  A sieve $\ssieve$ is \emph{finitely generated} if $\ssieve$ can be represented as $\ssieve = \left[V_1,\ldots,V_n\right]$.
\end{defn}

\begin{cor}
  \label{cor:bigger-allsieve}
  Let $\ccat$ be a category, $\topt$ a Grothendieck topology on $\ccat$ and $X$ an object of $\ccat$.
  Then 
  \[
    \left[\objects(\ccat_{/X})\right]
    \seq_{\topt}
    X, 
  \]
  where $\ccat_{/X}$ is the over-category of $X$.
  Explicitly: The sieve generated by the morphisms with common codomain $X$ is a $\topt$-cover of $X$.
\end{cor}
\begin{proof}
  Indeed, the sieve $[\objects(\ccat_{/X})]$ contains by definition all morphsism $Y' \to  X$ that factor as $Y' \to Y\to X$ for $ Y\to X $ some morphism in $\ccat$.
  This contains the all-sieve on $X$, and so by GT-Triv and \cref{fact:sieves-basic},\ref{fact:sieves-basic:contain}, $[\objects(\ccat_{/X})]$ is itself a $\topt$-covering.
\end{proof}
\begin{rem}
  More generally, we consider Grothendieck topologies on a topology base\footnote{Recall that a \emph{base} of a topological space is a collection $\baseb$ of open subsets such that every open subset of $X$ can be written as a union of some subset of elements of $\baseb$} $\mathcal{B}$ for $X$, considered as a poset with partial order given by inclusion.
  We will then employ the notation $\ssieve \seq \Omega$, $\ssieve \cslash \Omega$ (or $\mathcal{S} \seq_{\topt}$ or $\ssieve \cslash_{\topt} \Omega$) as before, prefering the upper-case greek letters $\Omega$, $\Theta$, $\Xi$, or $\Lambda$ or $\Gamma$ for elements of $\mathcal{B}$.
\end{rem}

\begin{defn}
  Let $X$ be a topological space, $\baseb$ a base for the topology on $X$ and $(\Omega_i)_{i \in I}$ an $I$-indexed collection of elements $\Omega_i \in \baseb$.
  Let $\left[\Omega_i ~|~ i \in I\right]_{\baseb}$ be the $\baseb$-sieve over $X$ generated by the $\Omega_i$, i.e. 
  \[
    \Set{\Theta \in \baseb \given \text{$\Theta\sse \Omega$ for at least one $i\in I$}}
    = 
    \bigcap_{\substack{\text{$\ssieve$ a $\baseb$-sieve }\\ \ssieve \supseteq \Set{\Omega_i \given i \in I}}}
    \ssieve. 
  \]
  The notion of being finitely generated will be used as before.
  The subscript $\baseb$ will always be used when $\baseb\subsetneq \mathfrak{O}_X$.
  We will write $\baseb_V$ for the set of all elements of $\baseb$ which are subsets of $V$.
  This is a base for the topology on $V$.
\end{defn}

\begin{prop}
  \label{prop:gtop-on-base}
  Let $X$ be a topological space, and $\baseb$ a topology base for $X$.
  Then we have a canonical bijection between:
  \begin{enumerate}
    \item 
      Grothendieck topologies $\topt_{\baseb}$ on $\baseb$;
    \item 
      \label{prop:gtop-on-base:on-all}
      Grothendieck topologies $\topt$ on $\mathfrak{O}_X$ such that for every open subset $U \sse X$, the relation $\left[\baseb_U\right]_{\mathfrak{O}} \seq_{\topt} U$ is satisfied.
  \end{enumerate}
  If $\topt_{\baseb}$ is given, $\topt$ is defined by 
  \[
    \ssieve \seq_{\topt} V ~\text{iff}~ \ssieve \cap \baseb_{\Omega} \seq_{\topt_{\baseb}} \Omega~ \text{for all $\Omega \in \baseb_{V}$}.
  \]
  If $\topt$ is given, $\topt_{\baseb}$ is defined by 
  \[
    \mathcal{S} \seq_{\topt_{\baseb}} \Omega 
    ~\text{iff}~
    [\ssieve] \seq_{\topt} \Omega.
  \]
\end{prop}

\begin{proof}
  We assume for simplicity that $\baseb$ is closed under finite non-empty intersections\footnote{So we do not assume that $\baseb$ is closed under the empty intersection, i.e. we do not assume $X\in \baseb$. This additional condition will be important later.}, i.e. if $\Omega,\Theta\in \baseb$, then $\Omega \cap \Theta \in \baseb$ as well.
  Let $\topt_{\baseb}$ be given.
  We need to show that $\topt$ as defined above is indeed a Grothendieck topology, and that is satisfies the condition imposed in \ref{prop:gtop-on-base:on-all}.
  \par
  Let us start with the latter:
  Let $U \sse X$ be an open subset.
  Then by definition, $[\baseb_U]_{\opo} \seq_{\topt}$ if and only if $[\baseb_U]_{\opo} \cap \baseb_{\Omega} \seq_{\topt_{\baseb}} \Omega$ for all $\Omega \in \baseb_U$.
  Explicitly, we have 
  \[
    [\baseb_U]_{\opo} \cap \baseb_{\Omega} \seq_{\topt_{\baseb}} \Omega
    = 
    \Set{V \in \baseb_{\Omega} \given V \subseteq \Theta \text{ for some $\Theta \in \baseb_U$}}.
  \]
  Call this sieve $\tsieve$ for the moment.
  Since $\Theta \in \baseb_U$, we have in have that $\tsieve$ contains $\baseb_{\Omega}$ as a subset, and so also $[\baseb_{\Omega}]_{\baseb}$, and the latter is a $\topt_{\baseb}$-covering by \cref{cor:bigger-allsieve}.
  \par 
  Let us now check that $\topt$ is indeed a Grothendieck topology on $\ouvcat(X)$:
  \begin{itemize}
    \item 
      GT-Triv is trivially satisfied, since for $V \sse X$ an open subset, $\asieve_V \cap \baseb_{\Omega}$ is the all-$\baseb$-sieve on $\Omega$ for $\Omega \in \baseb_V$, which covers $\Omega$ by GT-Triv for $\topt_{\baseb}$.
    \item 
      To check GT-Trans, let $\ssieve$ be a sieve with $\ssieve \seq_{\topt} V$, i.e. $\ssieve \cap \baseb_{\Omega} \seq_{\topt_{\baseb}} \Omega$ for all $\Omega \in \baseb_V$.
      Let $U \sse V$ be an open subset.
      We need to show that the pull-back sieve $\restrict{\ssieve}{U}$ covers $U$.
      Note that we have $\baseb_U \sse \baseb_V$, and thus for every $\Theta \in \baseb_U$, 
      \[
        \restrict{\ssieve}{U}
        \cap 
        \baseb_{\Theta} 
        \seq_{\topt_{\baseb}}
        \Theta
      \]
      holds, and so indeed $\restrict{S}{U} \seq_{\topt} U$.
    \item 
      For GT-Loc, let $\tsieve$ be a sieve in $V$, and let $\ssieve$ be a $\topt$-covering sieve of $V$, such that for all $U \in \ssieve$, the pull-back sieve $\restrict{\tsieve}{U}$ is a $\topt$-covering of $U$.
      Let $\Omega \in \baseb_V$.
      We need to see that 
      \begin{equation}
        \label{eq:baseproof-1}
        \tag{$\ast$}
        \tsieve \cap \baseb_{\Omega} \seq_{\topt_{\baseb}} \Omega 
      \end{equation}
      holds.
      We do this by using GT-Loc for $\topt_{\baseb}$.
      By definition, we have that the sieve $\ssieve \cap \baseb_{\Omega}$ covers $\Omega$ in $\topt$ ($\ssieve \cap \baseb_{\Omega} \seq_{\topt_{\baseb}} \Omega$), and by GT-Loc for $\topt_{\baseb}$, the relation \eqref{eq:baseproof-1} is satisfied as soon as $\restrict{(\tsieve \cap \baseb_{\Omega})}{\Theta} \seq_{\topt_{\baseb}} \Theta$ for every $\Theta \in \ssieve \cap \baseb_{\Omega}$. 
      The crucial point now is the identification 
      \[
        \restrict{(\tsieve \cap \baseb_{\Omega})}{\Theta}
        = 
        \restrict{\tsieve}{\Omega} 
        \cap 
        \baseb_{\Theta},
      \]
      and by assumption on $\tsieve$, this is indeed the case.
  \end{itemize}
  For the other direction, let $\ssieve$ be a $\baseb$-sieve in $\Omega$, then $\left[\ssieve\right] \seq_{\topt} \Omega$ if and only if $\left[\ssieve\right] \cap \baseb_{\Theta} \seq \Theta$ for all $\Theta \in \baseb_{\Omega}$.
  But $\left[\ssieve\right] \cap \baseb_{\Theta} = \restrict{\ssieve}{\Theta}$ thence ($\Theta = \Omega$) $\left[\ssieve\right] \seq_{\topt} \Omega$ implies $\ssieve \seq_{\topt_{\baseb}} \Omega$, and also $\ssieve \seq_{\topt_{\baseb}} \Omega$.
  Thus, $\topt_{\baseb}$ is recovered from $\topt$ by the above recipe.
  (Note that so far, we did not need the assumption that $\baseb$ is closed under intersections.)
  If we start with a Grothendieck topology $\topt$ on $\mathfrak{O}_X$, then GT-Triv for $\topt_{\baseb}$ is a again trivial as $[\ssieve]$ is the all-$\mathfrak{O}_X$-sieve in $\Omega$ and hence $\ssieve$ is the all-$\baseb$-sieve in $\Omega$.
  For GT-Loc and GT-Trans, note first that our assumption implies 
  \begin{equation}
    \tag{$\boxtimes$}
    \label{inproof:restrict-cond}
    \restrict{\left[\ssieve\right]}{\Theta}
    = 
    \left[\restrict{\ssieve}{\Theta}\right].
  \end{equation}
  For GT-Trans, let $\ssieve \seq_{\topt_{\baseb}} \Omega$ (so by definition, $\left[\ssieve\right] \seq_{\topt} \Omega$), and if $\Theta \in \baseb_\Omega$, then by first applying \eqref{inproof:restrict-cond} and then GT-Loc, we have 
  \[
    \left[\restrict{\ssieve}{\Theta}\right]
    = 
    \restrict{\left[\ssieve\right]}{\Theta}
    \seq_{\topt}
    \Theta.
  \]
  Thus, $\restrict{\ssieve}{\Theta} \seq_{\topt_{\baseb}} \Theta$, verifying GT-Trans for $\topt_{\baseb}$.
  For GT-Loc, let $\ssieve \seq_{\topt_{\baseb}} \Omega$ (hence $\left[\ssieve\right] \seq_{\topt} \Omega$), and let $\topt$ be a $\baseb$-sieve in $\Omega$, such that $\restrict{\tsieve}{\Theta} \seq_{\topt_{\baseb}} \Theta$ for all $\Theta \in \ssieve$.
  Hence, by \eqref{inproof:restrict-cond} and definition of $\topt_{\baseb}$, we have 
  \[
    \restrict{\left[\tsieve\right]}{\Theta} = \left[\restrict{\tsieve}{\Theta}\right] \seq_{\topt} \Theta .
  \]
  By GT-Trans for $\topt$, we have 
  \begin{equation}
    \tag{$\dagger$}
    \label{inproof:dagger}
    \restrict{\left[\tsieve\right]}{V}
    \seq_{\topt} 
    U 
  \end{equation}
  for all $U \in \left[\ssieve\right]$.
  As $\left[\ssieve\right] \seq_{\topt} \Omega$, \eqref{inproof:dagger} implies $\left[\tsieve\right] \seq_{\topt} \Omega$ by GT-Loc for $\topt$, hence $\tsieve \seq_{\topt_{\baseb}} \Omega$ by definition of $\topt_{\baseb}$, verifying GT-Loc for $\topt_{\baseb}$.
  It remains to check that we can recover $\topt$ from $\topt_{\baseb}$:
  For this, let $\topt'$ be the Grothendieck topology on $\opo_X$ which is obtained from $\topt_{\baseb}$ by the receipe from the Proposition.
  Let $\ssieve \seq_{\topt} V$.
  To show $\ssieve \seq_{\topt'}V$, we hat to show that 
  \[
    \ssieve \cap \baseb_{\Omega} \seq_{\topt_{\Omega}} \Omega
  \]
  for all $\Omega \in \baseb_{U}$, or $\left[\ssieve \cap \baseb_{\Omega}\right] \seq_{\topt} \Omega$ for all such $\Omega$.
  But $\left[\ssieve \cap \baseb_{\Omega}\right] = \restrict{\left[\ssieve \cap \baseb\right]}{\Omega}$, and this covers $\Omega$ by GT-Trans for $\topt$.
  Let $\ssieve \seq_{\topt'} V$, i.e. $\left[\ssieve \cap \baseb_{\Omega}\right] \seq_{\topt}\Omega$ for all $\Omega \in \baseb_V$.
  Thus, $\ssieve \cap \opo_{\Omega}$....
\end{proof}

\begin{defn}
  Let $\baseb$ be a topology base on a topological space $X$.
  By a \emph{$G_+$-topology on $\baseb$} we understand a Grothendieck-topology on $\baseb$ with the following additional assumption:
  \begin{enumerate}
    \item 
      If $\ssieve \seq \Omega$ then $\Omega = \bigcup_{\Theta \in \ssieve} \Theta$;
    \item 
      The empty sieve covers the empty set;
    \item 
      The topological on $X$ generated by $\baseb$ is $T_0$;
  \end{enumerate}
  If $\baseb = \opo_X$ is the topology base of all open subsets of $X$, then we speak just of the \emph{$G_+$-topology on $X$}.
\end{defn}

\begin{rem}
  Unless a coarser pretopology is considered, we call an open covering $U$ of $V$ \emph{admissible} for the $G_+$-topology under consideration if $[U]\seq V$. 
  This actually is one definition for the finest pretopology for $\topt$.
\end{rem}

\begin{example}[for $G_+$-topologies]
  \leavevmode 
  \begin{enumerate}
    \item 
      Ordinary topologies, i.e. $\ssieve \seq V$ if and only if $(\bigcup_{U\in \ssieve} U) = V$.
    \item 
      If $V$ is an open subset of a $G_+$-topological space $X$ then it caries an induced $G_+$-topology:
      \begin{itemize}
        \item 
          the underlying $\opo_V$ is given by $\Set{U \given\text{$U$ open in $X$, $U \sse V$}}$,
        \item 
          $\ssieve \seq_{\topt_V} U$ if and only if $\ssieve \seq_{\topt_X} U$ for $U \in \opo_V \sse \opo_X$.
      \end{itemize}
    \item 
      Let $\baseb$ be closed under finite intersections (i.e. $\Omega,\Theta \in \baseb \implies \Omega \cap \Theta \in \baseb$).
      Let $\ssieve \seq \Theta$ if and only if there are finitely many $\Theta_1, \ldots, \Theta_n \in \ssieve$ such that $\Omega = \bigcup_{i=1}^n \Theta_i$.
      Then this is a $G_+$-topology on $\baseb$:
      \begin{itemize}
        \item 
          GT-Trans:
          Let $\ssieve \seq \Omega$ and $\Theta_1,\ldots,\Theta_n$ and $\Xi \in \baseb_{\Omega}$ then $\Xi = \bigcup_{i=1}^n (\Theta_i \cap \Xi)$, and each term in an element of $\restrict{\ssieve}{\Xi}$, hence $\restrict{\ssieve}{\Xi} \seq \Xi$.
        \item 
          GT-Loc:
          Let $\ssieve \seq \Omega$, $\Theta_1,\ldots,\Theta_n$ as above and $\tsieve$ another sieve such that $\tsieve \cslash \Theta$ for all $\Theta \in \ssieve$.
          Hence ($\Theta = \Theta_i$), $\Theta_i = \bigcup_{j=1}^{n_i} \Theta_{i,j}$ with $\Theta_{i,j} \in \tsieve$.
          So $\Omega = \bigcup_{i=1}^n \bigcup_{j=1}^{n_i} \Theta_{ij}$ hence $\tsieve \seq \Omega$.
      \end{itemize}
      The additional conditions for being a $G_+$-topology are easy.
  \end{enumerate}
\end{example}

\begin{defn}
  Let $X$ be a $G_+$-topological space.
  A \emph{$G_+$-topology base for $X$} is an ordinary topology base $\baseb$ for the underlying ordinary topology, such that additionally, $\left[ \baseb_V\right] \seq V$ holds for all $V \in \opo_X$.
  The topology base is called $G_{++}$ if, in addition, membership in $\baseb$ is local in the following sense:
  \begin{enumerate}
    \item 
      If $\Omega,\Theta \in \baseb$, then $\Omega \cap \Theta \in \baseb$.%\footnote{Thus, for $V\in \opo_{\Omega}$, the set  $\Set{\Theta \in \baseb_{\Omega}\given V \cap \Theta \in \baseb$} is a sieve}
    \item 
      If $\Omega \in \baseb$ and $V \in \opo_{\Omega}$ such that 
      $ 
        \Set{\Theta \in \baseb_{\Omega} \given \Omega \cap V \in \baseb} 
        \seq 
        \Omega,
      $
      then $V \in \baseb$.
  \end{enumerate}
\end{defn}

\begin{cor}[to \cref{prop:gtop-on-base}]
  If $\baseb$ is a topology base on the topological space $X$, then there is a bijection between:
  \begin{enumerate}
    \item 
      $G_+$-topologies on $\baseb$;
    \item 
      $G_+$-topology on $X$ for which $\baseb$ is a $G_+$-topology base.
  \end{enumerate}
  If, in addition $\baseb$ is closed under finite intersections, then there is a unique $G_+$-topology on $X$ with the property that a covering of an element of $\baseb$ is admissible if and only if it has a finite subcovering.
\end{cor}

\begin{defn}
  This $G_+$-topology from the corollary is called the $G_+$-topology obtained by forcing the elements of $\baseb$ to be quasi-compact.
\end{defn}

\begin{defn}
  Let $K$ be a real closed field.
  The \emph{Delfs-Knebusch topology} is the following $G_+$-topology on $K^n$:
  \begin{enumerate}
    \item
      $V\sse K^n$ is open if and only if for all $x\in V$, there is an $\eepsilon \in (0,\infty)_K$ such that 
      \[
        \Set{y \in K^n \given \sum_{i=1}^n (x_i - y_i)^2 < \eepsilon^2 } \sse V.
      \]
    \item 
      The $G_+$-topology is obtained by enforcing the quasi-compactness of open subsets of the form
      \[
        P_{K^n}
        (f_1,\ldots,f_n)
        = 
        \Set{x\in K^n \given f_1(x) > 0, \ldots, f_m(x) > 0} 
      \]
      for $f_i \in K[X_1,\ldots,X_n]$.
  \end{enumerate}
\end{defn}

\begin{rem}
  Delfs and Knebusch actually work with pretopologies.
  In any case, the cohomology on such topological spaces behaves as expected.
\end{rem}

\begin{defn}
  \label{defn:prime-sieve}
  Let $\baseb$ be a topology base, closed under arbitrary finite intersections (including the empty one, i.e. $X \in \baseb$)\footnote{In everything before this line, $\baseb$ being closed under intersections should be replaced by $\Omega,\Theta \in \baseb \implies \Omega \cap \Theta \in \baseb$, i.e. we do not require $X \in \baseb$.}
  Then a sieve $\ssieve \in \baseb$ is called a \emph{prime sieve} if and only if for all $\Omega_1,\ldots,\Omega_N \in \baseb$ such that $\bigcap_{i=1}^N \Omega_i \in \ssieve$ holds it follows that there is an $1\leq i \leq N$ such that $\Omega_i \in \ssieve$.
\end{defn}

\begin{rem}
  Obviously, $\ssieve$ is a prime sieve if and only if the following two conditions are satisfied:
  \begin{enumerate}
    \item 
      $X \notin \ssieve$;
    \item 
      For $\Omega,\Theta\in \baseb$, we have $\Omega \cap \Theta \in \ssieve$ if and only if $\Omega \in \ssieve$ or $\Theta \in \ssieve$.
  \end{enumerate}
\end{rem}

\begin{rem}
  An appropriate modification for arbitrary topology bases could be:
  For $\Omega_1,\ldots,\Omega_N \in \baseb$ and $\Theta \in \ssieve$ such that $\Theta \sse \bigcap_{i=1}^M \Omega_i$, then there is a $1 \leq i \leq N$ such that $\Omega_i \in \ssieve$.
\end{rem}

\begin{prop}
  Let $\baseb$ be a $G_+$-topology base for a $G_+$-topological space $X$ that is closed under arbitrary finite intersections of its elements.
  Consider the following conditions on a subset $\xi \sse \baseb$:
  \begin{enumerate}
    \item 
      If $\Omega \in \xi$ and $\Omega \sse \Theta $ then $\Theta \in \xi$.
    \item 
      A finite intersection in $X$ of elements of $\xi$ is an element of $\xi$ (in particular, $X \in \xi$).
    \item 
      If $\Omega \in \xi$ and $\ssieve \seq \Omega$, then $\ssieve \cap \xi \neq \emptyset$ (in particular, taking $\ssieve = \emptyset$, we have $\emptyset \notin \xi$).
  \end{enumerate}
  Then conditions (i), (ii) and (iii) together are equivalent to $\ssieve = \baseb \setminus \xi$ being a prime sieve containing every element $\Omega$ of $\baseb$ with $\ssieve \cslash \Omega$.
\end{prop}

\begin{defn}
  Let $\baseb$ be a $G_+$-topology base such that $\Omega,\Theta \in \baseb \implies \Theta \cap \Omega \in \baseb$.
  A \emph{van der Put-point} for $\baseb$ is a subset $\xi \sse \baseb$ such that 
  \begin{enumerate}
    \item 
      For $\Omega \in \xi$ and $\Theta \in \baseb$ such that $\Omega \sse \Theta$, we have $\Theta \in \xi$.
    \item 
      If $\Theta,\Omega \in \xi$ then $\Omega \cap \Theta \in \xi$.
      Moreover, $\xi \neq \emptyset$.
    \item If $\Omega \in \xi$ and $\ssieve \seq \Omega$, then $\ssieve \cap \xi \neq \emptyset$.
  \end{enumerate}
\end{defn}

\begin{fact}
  \leavevmode 
  \begin{enumerate}
    \item 
      If $\baseb$ is a $G_+$-topology base of a $G_+$-topological space $X$, then any $\baseb \sse \wtilde{\baseb} \sse \opo_X$ is also a $G_+$-topology base.
    \item 
      If $V \sse X$ is an open subset and $\baseb$ is a $G_+$-topology base for $X$, then $\baseb_V$ is a $G_+$-topology base for $V$.
    \item 
      Let $\baseb \sse \opo_X$ be a collection of open subsets of $X$, where $X$ is a $G_+$-topological space.
      Consider the sieve $\ssieve$ consisting of all open subsets $V \sse X$ such that $\baseb_V$ is a $G_+$-topology base for $V$.
      If $\ssieve$ covers $X$, then $\baseb$ is a $G_+$-topology base for $X$.
  \end{enumerate}
\end{fact}

We have to modify the definition of a prime sieve, the previous one being somewhat incorrect:\footnote{Now copied verbatim}

\begin{defn}
  Let $\baseb$ be any topology base on a topoological space $X$\footnote{So no assumption on intersections for $\baseb$}.
  A sieve $\ssieve$ in $\baseb$ is a \emph{prime sieve} if for all $N \in \nn$, $(\Omega_i)_{i=1}^N$ and $\baseb_{\bigcap_{i=1}^N \Omega_i} \sse \ssieve$ implies the existence of $i \in \zz \cap [1,N]$ such that $\Omega_i \in \ssieve$.
\end{defn}

\begin{rem}
  \leavevmode 
  \begin{enumerate}
    \item 
      In particular ($N= 0$) $\ssieve$ is not the all-sieve and hence $X \notin \ssieve$.
    \item 
      A sieve is a prime sieve if it satisfies the condition for $N = 0$ (i.e. $\ssieve$ is not the all-sieve) and $N=2$.
    \item 
      If $\Omega \cap \Theta \in \baseb$ for all $\Omega,\Theta \in \baseb$, then $\ssieve$ is a prime sieve if and only if the following two conditions are satisfied:
      \begin{enumerate}
        \item 
          $\ssieve$ is not the all-sieve; and 
        \item 
          If $\Omega, \Theta \in \baseb \setminus \ssieve$, then $\Omega \cap \Theta \notin \ssieve$.
      \end{enumerate}
      If $X \in \baseb$, condition a) is equivalent to $X\in \baseb$.
  \end{enumerate}
\end{rem}

\begin{fact}
  Let $\baseb$ be a $G_+$-topology base on a $G_+$-space $X$.
  Let $\Omega,\Theta \in \baseb$.
  Consider the following conditions on a subset $\xi \sse \baseb$:
  \begin{enumerate}
    \item 
      If $\Omega \in \xi$ and $\Omega \sse \Theta $ then $\Theta \in \xi$.
    \item 
      A finite intersection in $X$ of elements of $\xi$ contains some element of $\xi$.
    \item 
      If $\Omega \in \xi$ and $\tsieve \seq \Omega$, then $\tsieve \cap \xi \neq \emptyset$ (in particular, taking $\ssieve = \emptyset$, we have $\emptyset \notin \xi$).
  \end{enumerate}
  Then the conditions (i), (ii) and (iii) together are equivalent to $\ssieve = \baseb \setminus \xi$ being a prime sieve, and $\ssieve \cslash \Omega$ implies $\Omega \in \ssieve$.
\end{fact}

\begin{proof}
  The condition (i) is equivalent to $\ssieve$ being a sieve.
  Conditions (i) and (ii) together are equivalent to $\ssieve$ being a prime sieve.
  Finally, conditions (i), (ii) and (iii) together are equivalent to $\ssieve$ being a prime sieve containing everything it covers.
\end{proof}

\begin{defn}
  A \emph{van der Put-point} for a $G_+$-topology base is a subset $\xi \sse \baseb$ satisfying these equivalent conditions.
\end{defn}

\begin{rem}
\leavevmode 
  \begin{enumerate}
    \item 
      If $\baseb$ satisfies $\Omega,\Theta \in \baseb \Rightarrow \Omega \cap \Theta \in \baseb$, then $\xi \sse \baseb$ is a van der Put-point if and only if it satisfies the following conditions:
      \begin{enumerate}
        \item 
          $\Omega \in \Xi$, $\Theta \supseteq \Omega$ implies $\Theta \in \Xi$,
        \item 
          $\Omega,\Theta \in \xi$ implies $\Omega \cap \Theta \in \xi$,
        \item 
          $\xi \neq \emptyset$.
        \item 
          $\Omega \in \xi$, $\ssieve \seq \Omega$ omplies $\ssieve \cap \xi \neq \emptyset$.
      \end{enumerate}
    \item 
      Let $X^\ast_\baseb$ be the set of van der Put-points for $\baseb$.
      If $\baseb = X$, we will abbreviate these to $X^\ast$.
    \item 
      For a van der Put-point $\xi$, we set $\ssieve_\xi \defo \baseb \setminus \xi$ and $\xi_{\ssieve} \defo \baseb \setminus \ssieve$.
  \end{enumerate}
\end{rem}

\begin{example}
  Every ordinary point of $x\in X$ defines a van der Put-point of $X$, by setting 
  \[
    \xi_x
    = 
    \Set{\Omega \in \baseb \given x\in \Omega}.
  \]
  As $X$ is assumed to be $T_0$, this gives an injective map $X \hookrightarrow X^\ast$, and we will always think of $X$ as being a subset of $X^\ast$.
\end{example}

\begin{rem}
  If $X$ is a $G_+$-topological space, and $\baseb$ is a $G_+$-topology base, then we have a canonical bijection 
  \[
    X_{\baseb}^\ast 
    \cong 
    X^\ast,
  \]
  via $\xi_{\baseb} \mapsto \xi_{\opo} = \Set{V \sse X \given \baseb_V \cap \Xi \neq \emptyset}$, with inverse given by $\xi_{\opo} \mapsto \xi_{\baseb} = \baseb \cap \xi_{\opo}$.
  For sieves, there is a similar correspondence $\ssieve_{\baseb} \mapsto \ssieve_{\opo} = \left[\ssieve_{\baseb}\right]$ and $\ssieve_{\opo} \mapsto \ssieve_{\baseb} = \baseb \cap \ssieve_{\opo}$.
\end{rem}

\begin{rem}
  Let $V \sse X$ be an open subset.
  We have a bijection 
  \begin{equation}
    V^\ast \isomorphism \Set{v\in X^\ast \given V\in v}
    ,~
    \xi 
    \mapsto 
    v = \Set{U\in \opo_X \given V \cap U \in \xi},
  \end{equation}
  with inverse given by $v \mapsto v \cap \opo_V$.
  Under this identification, we have in $X^\ast$ the relation 
  \begin{equation}
    (v \cap U)^\ast = V^\ast \cap U^\ast .
  \end{equation}
  We thus have a topology base $\Set{V^\ast \given V \in \opo_X}$ and we use this to equip $X^\ast$ with a topology.
  From now on, we will identify $V^\ast$ with the above subset of $X^\ast$.
  If we work with a $G_{+}$-topology base $\baseb$, the bijection becomes 
  \[
    V_{\baseb}^\ast 
    \cong 
    \Set{v \in X^\ast_{\baseb} \given \baseb_V \cap v \neq \emptyset},
  \]
  induced by $\xi \mapsto \Set{\Omega \in \baseb \given \baseb_{V \cap \Omega} \cap \xi \neq \emptyset}$, and $v \mapsto v \cap \baseb_V$. 
  Moreover, $\Set{\Omega^\ast \given \Omega \in \baseb}$ is a topology base for $X^\ast$.
\end{rem}

\begin{rem}
  \leavevmode
  \begin{enumerate}
    \item 
      In $X^\ast$, we have $\xi \in \overline{\Set{v}}$ if and only if $\xi \sse v$.
    \item 
      The closed points of $X^\ast$ are in canonical bijection with the maximal non-covering sieves in $X$.
  \end{enumerate}
\end{rem}

\begin{proof}
  Only the proof of maximal non-covering sieves is non-trivial.
  Obviously, such an $\ssieve$ cannot be the all-sieve (by GT-Triv).
  Let $\Omega,\Theta \in \ssieve$, then $\ssieve[\Omega] \defo \ssieve \cup [\Omega]$ and $\ssieve[\Theta]$ cover $X$.
  Thus, for $\Xi \in \baseb$, we have $\ssieve[\Theta] \sslash \Xi$, but when $\Xi \in \ssieve[\Omega]$ then $\restrict{ssieve[\Theta]}{\Xi} = \restrict{\ssieve}{\Xi}$, provided that $\baseb_{\Omega \cap \Theta} \sse \ssieve$.
  Hence, by GT-Loc, $\baseb_{\Omega \cap \Theta} \sse \ssieve$ would imply $\ssieve \seq X$, a contradiction.
\end{proof}

\begin{prop}
  Let $X$ be an arbitrary $T_0$ space ($\ssieve \seq V$ if and only if $V = \bigcup_{U \in \ssieve} U$), then we have a bijection 
  \[
    X^\ast 
    \isomorphism 
    \Set{Z \sse X \given \text{$Z$ is an irreducible subset}},
  \]
  given by $\xi \mapsto X \setminus \bigcup_{V \in \ssieve_{\xi}} V$, with inverse given by $Z \mapsto \Set{V \in \opo_X \given V \cap Z \neq \emptyset}$.
  In particular, we have $\ssieve_{\xi} = \opo_{X \setminus Z}$.
\end{prop}

\begin{proof}
  It is easy to verify (i) til (iii) for $\xi$:
  \begin{enumerate}
    \item 
      If $V \in \xi$, then $V \cap Z \neq \emptyset$, and thus $U \cap Z \neq \emptyset$ whenever $U \supseteq V$.
    \item 
      If $V,U\in \xi$, then $V\cap Z$ and $U \cap Z$ are both non-empty.
      Since $Z$ is irreducible, we have 
      \[
        \emptyset 
        \neq 
        (V \cap Z) 
        \cap 
        (U \cap Z) 
        = 
        (U \cap V) \cap Z,
      \]
      whence $V \cap U  \in \xi$.
      Also $X \in \xi$, as $Z \neq \emptyset$ (irreducible sets are non-empty, by definition).
    \item 
      If $V \in \xi$ and $V = \bigcup_{U \in \ssieve} U$, then there is $U \in \ssieve$ such that $U \cap Z \neq \emptyset$, hence $U \in \xi$.
      If $\xi \in X^\ast$ (hence $\ssieve = \opo_X \setminus \xi$ is a prime sieve such that $U \in \ssieve$ when $\ssieve \sslash U$), then $V = \bigcup_{U \in \ssieve} U$ is open and $\ssieve \sslash V$, hence $V \in \ssieve$ and $\ssieve = \opo_V$.
      The prime sieve property is equivalent to $Z = X \setminus V$ being irreducible:
      $Z = \emptyset$ would imply $\ssieve = \opo_X$. 
      If $Z = Z_1 \cup Z_2$ with both $Z_1,Z_2 \subsetneq Z$ closed, then $(X\setminus Z_1) \cap (X\setminus Z_2) = V \in \ssieve$, while $X\setminus Z_i \notin \ssieve$.
  \end{enumerate}
  That we recover $Z$ from $\xi$ follows from $\ssieve_{\xi} = \opo_{X\setminus Z}$, hence $\bigcup_{U \in \ssieve_{\xi}} U = X \setminus Z$.
  The fact that we recover $\xi$ from $Z$ follows from is easy.
\end{proof}

\begin{rem}
  In a topological space, $\overline{\Set{x}}$ is always irreducible.
\end{rem}

\begin{defn}
  A topological space is \emph{sober} if every irreducible subset has a unique generic point.
  A \emph{spectral space} is a sober space such that the set $\baseb$ of quasi-compact open subsets of $X$ is a topology base, which is closed under arbitrary finite (including empty) intersections in $X$.
\end{defn}

\begin{rem}
  By the uniqueness part of the definition, sober spaces are $T_0$.
  Taking the empty intersection shows that spectral spaces themselves are always quasi-compact.
\end{rem}

\begin{cor}
  For ordinary topological spaces, we have $X=X^\ast$ if and only if $X$ is sober.
\end{cor}


\thispagestyle{plain}
\printbibliography


%\listoftheorems[ignoreall,
%show={todo, danger}]
\end{document}
